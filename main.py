def sum_numbers(*args):
    return sum(args)


def get_multiplied_amount(multiplier, *args):
    return sum(args) * multiplier


def word_concatenator(*args):
    return ' '.join(args)
    

def inverted_word_factory(*args):
    inverted_words = ''
    words_list = list(args)
    words_list.reverse()

    for word in words_list:
        inverted_word = word[::-1]
        inverted_words += inverted_word + ' '
    
    return inverted_words.rstrip()


def dictionary_separator(**kwargs):
    keys = list(kwargs.keys())
    values = list(kwargs.values())
    
    return (keys, values)


def dictionary_creator(*args, **kwargs):
    new_dict = {}

    for i, k in enumerate(kwargs.keys()):
        new_dict.update({args[i]: kwargs[k]})

    return new_dict


def purchase_logger(**kwargs):
    return f'Product {kwargs["name"]} costs {kwargs["price"]} and was bought {kwargs["quantity"]}'

purchase = {"name": "washing powder", "price": 6.7, "quantity": 4}


def world_cup_logger(country, *args):
    sentence = ''
    
    args = [str(i) for i in args]
    args.reverse()

    for i, v in enumerate(args):
        print(i, v)
        if i == 0:
            sentence += v
        elif i == len(args) - 1:
            sentence += ' e ' + v
        else:
            sentence += ', ' + v

    return f'{country} - {sentence}'